<?php

// app/code/local/Keenu/Netconnectpaymentmethod/controllers/PaymentController.php
class Keenu_Netconnectpaymentmethod_PaymentController extends Mage_Core_Controller_Front_Action {

    protected $_order;

    public function gatewayAction() {
        if ($this->getRequest()->get("orderId")) {
            $arr_querystring = array(
                'flag' => 1,
                'orderId' => $this->getRequest()->get("orderId")
            );

            Mage_Core_Controller_Varien_Action::_redirect('netconnectpaymentmethod/payment/response', array('_secure' => false, '_query' => $arr_querystring));
        }
    }

    public function redirectAction() {
        $session = Mage::getSingleton('checkout/session');
        $session->setNetconnectpaymentmethodPaymentQuoteId($session->getQuoteId());
        $this->getResponse()->setBody($this->getLayout()->createBlock('netconnectpaymentmethod/standard_redirect')->toHtml());
        $session->unsQuoteId();
        $session->unsRedirectUrl();
        /* $this->loadLayout();
          $block = $this->getLayout()->createBlock('Mage_Core_Block_Template', 'netconnectpaymentmethod', array('template' => 'netconnectpaymentmethod/redirect.phtml'));
          $this->getLayout()->getBlock('content')->append($block);
          $this->renderLayout(); */
    }

    public function responseAction() {
        if ($this->getRequest()->get("flag") == "1" && $this->getRequest()->get("orderId")) {
            $orderId = $this->getRequest()->get("orderId");
            $order = Mage::getModel('sales/order')->loadByIncrementId($orderId);
            $order->setState(Mage_Sales_Model_Order::STATE_PAYMENT_REVIEW, true, 'Payment Success.');
            $order->save();

            Mage::getSingleton('checkout/session')->unsQuoteId();
            Mage_Core_Controller_Varien_Action::_redirect('checkout/onepage/success', array('_secure' => false));
        } else {
            Mage_Core_Controller_Varien_Action::_redirect('checkout/onepage/error', array('_secure' => false));
        }
    }

    public function initialize($paymentAction, $stateObject) {
        $state = Mage_Sales_Model_Order::STATE_PENDING_PAYMENT;
        $stateObject->setState($state);
        $stateObject->setStatus('pending_payment');
        $stateObject->setIsNotified(false);
    }

    protected function _expireAjax() {
        if (!Mage::getSingleton('checkout/session')->getQuote()->hasItems()) {
            $this->getResponse()->setHeader('HTTP/1.1', '403 Session Expired');
            exit;
        }
    }

    public function getStandard() {
        return Mage::getSingleton('netconnectpaymentmethod/payment');
    }

    public function cancelAction() {
        $session = Mage::getSingleton('checkout/session');
        
        $orderId = $session->getOrderID();
        $transactionId = $session->getTransactionID();
        $status = $session->getStatus();
        $amount = $session->getAmount();
        
        
        
        Mage::getModel('netconnectpaymentmethod/Paymentmethod')->registerPaymentFailure($orderId, $status);
        
        if ($orderId) {
            $order = Mage::getModel('sales/order')->loadByIncrementId($orderId);
            
            if ($order->getId()) {
                $order->cancel()->save();
            }
            Mage::helper('netconnectpaymentmethod/checkout')->restoreQuote();
        }
        $this->_redirect('checkout/onepage/Failure');
    }
    
    public function denialAction() {
        $session = Mage::getSingleton('checkout/session');
        
        $orderId = $session->getOrderID();
        $transactionId = $session->getTransactionID();
        $status = $session->getStatus();
        $amount = $session->getAmount();
        
        Mage::getModel('netconnectpaymentmethod/Paymentmethod')->registerPaymentFailure($orderId, $transactionId);
        
        if ($orderId) {
            $order = Mage::getModel('sales/order')->loadByIncrementId($orderId);
            if ($order->getId()) {
                $order->cancel()->save();
            }
            Mage::helper('netconnectpaymentmethod/checkout')->restoreQuote();
        }
        $this->_redirect('checkout/onepage/Failure');
    }

    public function successAction() {
        $session = Mage::getSingleton('checkout/session');
        //$session->setQuoteId($session->getNetconnectpaymentmethodPaymentQuoteId(true));
        
        $orderId = Mage::getSingleton('checkout/session')->getOrderID();
        $transactionId = Mage::getSingleton('checkout/session')->getTransactionID();
        $status = Mage::getSingleton('checkout/session')->getStatus();
        $amount = Mage::getSingleton('checkout/session')->getAmount();
        
        Mage::getModel('netconnectpaymentmethod/Paymentmethod')->registerPaymentSuccess($orderId, $transactionId, $status, $amount);
        
        
        Mage::getSingleton('checkout/session')->getQuote()->setIsActive(false)->save();
        //echo $orderId.'-'.$transactionId.'-'.$status.'-'.$amount.Mage::getSingleton('checkout/session')->getQuote();exit;
        $this->_redirect('checkout/onepage/success', array('_secure' => true));
    }

    public function returnAction() {
        
        $checksum = $this->getRequest()->get('CheckSum');
        $AUTH_STATUS_NO = $this->getRequest()->get('AUTH_STATUS_NO');

        $order_id = $this->getRequest()->get('Order_ID');
        $transaction_id = $this->getRequest()->get('Transaction_ID');
        $status = $this->getRequest()->get('Status');
        $date = $this->getRequest()->get('Date');
        $time = $this->getRequest()->get('Time');
        $Bank_Name = $this->getRequest()->get('Bank_Name');

        $order = Mage::getModel('sales/order')->loadByIncrementId($order_id);
        
        
        $amount = number_format($order->getGrandTotal(), 2, '.', '');
        $conchecksumm = Mage::getModel('netconnectpaymentmethod/Paymentmethod')->Verify_CheckSum(Mage::getModel('netconnectpaymentmethod/Paymentmethod')->getSecret(),
                Mage::getModel('netconnectpaymentmethod/Paymentmethod')->getMerchant(), $order_id, $amount, $Bank_Name, $status, $date, $time, $checksum);
        /*echo Mage::getModel('netconnectpaymentmethod/Paymentmethod')->getSecret()."|".Mage::getModel('netconnectpaymentmethod/Paymentmethod')->getMerchant()."|".$order_id."|".$amount."|".$Bank_Name."|".$status."|".$date."|".$time."|".$checksum.'<br/>';
        $New_CheckSum = Mage::getModel('netconnectpaymentmethod/Paymentmethod')->Generate_Response_CheckSum(Mage::getModel('netconnectpaymentmethod/Paymentmethod')->getSecret(),
                Mage::getModel('netconnectpaymentmethod/Paymentmethod')->getMerchant(), $order_id, $amount, $Bank_Name, $status, $date, $time);
        echo Mage::getModel('netconnectpaymentmethod/Paymentmethod')->getSecret()."|".
                Mage::getModel('netconnectpaymentmethod/Paymentmethod')->getMerchant()."|". $order_id."|". $amount."|". $Bank_Name."|". $status."|". $date."|". $time;
        echo"<pre>"; print_r($conchecksumm);exit;*/
        
        Mage::getSingleton('checkout/session')->setOrderID($order_id);
        Mage::getSingleton('checkout/session')->setTransactionID($transaction_id);
        Mage::getSingleton('checkout/session')->setStatus($status);
        Mage::getSingleton('checkout/session')->setAmount($amount);
        
        if ($conchecksumm == true) {
            if (trim($AUTH_STATUS_NO) == "01") {
                $this->successAction();
            } else if (trim($AUTH_STATUS_NO) == "02") {
                $this->cancelAction();
            } else if (trim($AUTH_STATUS_NO) == "03") {
                $this->denialAction();
            } else {
                $this->cancelAction();
            }
        }
    }

}
