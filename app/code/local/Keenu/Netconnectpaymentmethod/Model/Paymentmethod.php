<?php

// app/code/local/Keenu/Netconnectpaymentmethod/Model/Paymentmethod.php
class Keenu_Netconnectpaymentmethod_Model_Paymentmethod extends Mage_Payment_Model_Method_Abstract {

    protected $_code = 'netconnectpaymentmethod';
    protected $_isInitializeNeeded = true;
    protected $_canUseInternal = false;
    protected $_canUseForMultishipping = false;
    protected $_config = null;
    protected $_formBlockType = 'netconnectpaymentmethod/standard_form';
    protected $_infoBlockType = 'netconnectpaymentmethod/payment_info';
    protected static $merchant_id = "";
    protected static $secret = "";
    protected static $submit_url = "";

    //protected $_formBlockType = 'netconnectpaymentmethod/form_netconnectpaymentmethod';
    //protected $_infoBlockType = 'netconnectpaymentmethod/info_netconnectpaymentmethod';

    /* public function assignData($data) {
      $info = $this->getInfoInstance();

      if ($data->getCustomFieldOne()) {
      $info->setCustomFieldOne($data->getCustomFieldOne());
      }

      if ($data->getCustomFieldTwo()) {
      $info->setCustomFieldTwo($data->getCustomFieldTwo());
      }

      return $this;
      }

      public function validate() {
      parent::validate();
      $info = $this->getInfoInstance();

      if (!$info->getCustomFieldOne()) {
      $errorCode = 'invalid_data';
      $errorMsg = $this->_getHelper()->__("CustomFieldOne is a required field.\n");
      }

      if (!$info->getCustomFieldTwo()) {
      $errorCode = 'invalid_data';
      $errorMsg .= $this->_getHelper()->__('CustomFieldTwo is a required field.');
      }

      if ($errorMsg) {
      Mage::throwException($errorMsg);
      }

      return $this;
      }
     */
    public function __construct() {
        parent::__construct();
        //$this->_init('netconnectpaymentmethod/paymentmethod');
        $merchantId = (string) Mage::getSingleton('adminhtml/config_data')
                        ->getConfigDataValue('netconnectpaymentmethod/general/merchant_id');
        $sec = (string) Mage::getSingleton('adminhtml/config_data')
                        ->getConfigDataValue('netconnectpaymentmethod/general/merchant_secret');
        $submit_url = (string) Mage::getSingleton('adminhtml/config_data')
                        ->getConfigDataValue('netconnectpaymentmethod/general/submit_url');
        $this->setSubmitURL($submit_url);
        $this->setMerchant($merchantId);
        $this->setSecret($sec);
    }

    public function getOrderPlaceRedirectUrl() {
        return Mage::getUrl('netconnectpaymentmethod/payment/redirect', array('_secure' => false));
    }

    public function Generate_Request_CheckSum($Secret_Key, $Merchant_ID, $Order_ID, $Order_Amount, $Date, $Time) {
        $Concate_String = $Secret_Key . "|" . $Merchant_ID . "|" . $Order_ID . "|" . $Order_Amount . "|" . $Date . "|" . $Time;

        $HMac_MD5 = strtolower(hash_hmac('md5', $Concate_String, $Secret_Key));
        return $HMac_MD5;
    }

    public function Generate_Response_CheckSum($Secret_Key, $Merchant_ID, $Order_ID, $Order_Amount, $Bank_Name, $Transaction_Status, $Date, $Time) {
        $Concate_String = $Secret_Key . "|" . $Merchant_ID . "|" . $Order_ID . "|" . $Order_Amount . "|" . $Bank_Name . "|" . $Transaction_Status . "|" . $Date . "|" . $Time;

        $HMac_MD5 = strtolower(hash_hmac('md5', $Concate_String, $Secret_Key));
        return $HMac_MD5;
    }

    public function Verify_CheckSum($Secret_Key, $Merchant_ID, $Order_ID, $Order_Amount, $Bank_Name, $Transaction_Status, $Date, $Time, $CheckSum) {
        $New_CheckSum = $this->Generate_Response_CheckSum($Secret_Key, $Merchant_ID, $Order_ID, $Order_Amount, $Bank_Name, $Transaction_Status, $Date, $Time);
        if ($CheckSum === $New_CheckSum)
            return true;
        else
            return false;
    }

    public function getSession() {
        return Mage::getSingleton('netconnectpaymentmethod/session');
    }

    public function getCheckout() {
        return Mage::getSingleton('checkout/session');
    }

    public function getQuote() {
        return $this->getCheckout()->getQuote();
    }
    public function getQuoteId() {
        return $this->getCheckout()->getQuoteId();
    }
    public function setQuoteId($quoteid) {
        return $this->getCheckout()->setQuoteId($quoteid);
    }

    public function createFormBlock($name) {
        $block = $this->getLayout()->createBlock('netconnectpaymentmethod/standard_form', $name)
                ->setMethod('netconnectpaymentmethod_standard')
                ->setPayment($this->getPayment())
                ->setTemplate('netconnectpaymentmethod/standard/form.phtml');

        return $block;
    }

    public function getStandardCheckoutFormFields() {
        $orderIncrementId = $this->getCheckout()->getLastRealOrderId();
        $order = Mage::getModel('sales/order')->loadByIncrementId($orderIncrementId);
        $this->setQuoteId($order->getQuoteId());
        
        /* $order = new Mage_Sales_Model_Order();
          $orderId = Mage::getSingleton('checkout/session')->getLastRealOrderId();
          $order->loadByIncrementId($orderId); */


        //$conchecksumm = $this->Verify_CheckSum( $this->secret, $this->merchant_id, $order_id, $var_total, $Bank_Name, $status,$date,$time,$checksum);


        $amount = $order->getGrandTotal();
        $date = date("d/m/Y");
        $time = date("H:i:s");
        $ref_no = $order->getCustomerId();
        $checksum = $this->Generate_Request_CheckSum($this->secret, $this->merchant_id, $orderIncrementId, $amount, $date, $time);
        //echo "Secret:".$this->secret.'<br/>MerchantId:'.$this->merchant_id.'<br/>OrderId: '.$orderIncrementId.'<br/>Amount:'.$amount.'<br/>Date:'.$date.'<br/>Time:'.$time;
        //$data = array('Merchant_ID' => $this->merchant_id, 'Order_No' => $this->session->data['order_id'], 'Order_Amount' => $amount, 'CheckSum' => $checksum, 'Date' => $date, 'Time' => $time, 'Transaction_Desc' => 'order Desc');

        $request['Merchant_ID'] = $this->merchant_id;
        $request['Order_No'] = $orderIncrementId;
        $request['Order_Amount'] = $amount;
        $request['Unique_Ref_No'] = $ref_no;
        $request['CheckSum'] = $checksum;
        $request['Date'] = $date;
        $request['Time'] = $time;
        $request['Transaction_Desc'] = 'order Desc';
//echo "<pre>"; print_r($this->getQuoteId());exit;
        return $request;
    }

    public function setSubmitURL($url) {
        $this->submit_url = $url;
    }

    public function getSubmitURL() {
        return $this->submit_url;
    }

    public function setMerchant($merchantId) {
        $this->merchant_id = $merchantId;
    }

    public function getMerchant() {
        return $this->merchant_id;
    }

    public function setSecret($s) {
        $this->secret = $s;
    }

    public function getSecret() {
        return $this->secret;
    }

    public function registerPaymentSuccess($id, $transaction_id, $status, $amount) {
        $order = Mage::getModel('sales/order')->loadByIncrementId($id);

        $payment = $order->getPayment();
        $methodCode = $order->getPayment()->getMethod();
        $message = "Keenu Netconnect: " . $status;

        $payment->setTransactionId($transaction_id)
                ->setPreparedMessage($message)
                ->setIsTransactionClosed(0)
                ->registerCaptureNotification($amount);
        $order->save();

        // notify customer
        $invoice = $payment->getCreatedInvoice();
        if ($invoice && !$order->getEmailSent()) {
            $order->sendNewOrderEmail()->addStatusHistoryComment(
                            __('Notified customer about invoice #%s.', $invoice->getIncrementId())
                    )
                    ->setIsCustomerNotified(true)
                    ->save();
        }
    }

    public function registerPaymentDenial($id, $transaction_id) {
        $order = Mage::getModel('sales/order')->loadByIncrementId($id);

        $order->getPayment()
                ->setTransactionId($transaction_id)
                ->setNotificationResult(true)
                ->setIsTransactionClosed(true)
                ->registerPaymentReviewAction(Mage_Sales_Model_Order_Payment::REVIEW_ACTION_DENY, false);
        $this->_order->save();
    }

    public function registerPaymentFailure($id, $status) {
        $order = Mage::getModel('sales/order')->loadByIncrementId($id);

        $message = "Keenu Netconnect: " . $status;
        $order
                ->registerCancellation($message, false)
                ->save();
    }

}
