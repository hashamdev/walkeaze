<?php
// app/code/local/Keenu/Netconnectpaymentmethod/Block/Form/Netconnectpaymentmethod.php
class Keenu_Netconnectpaymentmethod_Block_Form_Netconnectpaymentmethod extends Mage_Payment_Block_Form
{
  protected function _construct()
  {
    parent::_construct();
    $this->setTemplate('netconnectpaymentmethod/form/netconnectpaymentmethod.phtml');
  }
}